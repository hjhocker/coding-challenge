# FAE Coding Challenge

## General Info

The idea behind this backend is it's a very basic wall/feed that users can post messages to. User authentication is done with HTTP Basic Authentication for simplicity's sake. Users have a privacy level of PUBLIC or PRIVATE but these currently do nothing. Users can also add other users as a friend, but this is one-directional. That is, if I add someone as my friend, that doesn't mean they've added me as their's.

The backend is written in Java using Spring Boot and is a REST API with all responses being JSON.

The database engine used is Postgres (https://www.postgresql.org/) and migrations are controlled using Flyway (https://flywaydb.org/).

The provided codebase is only meant to act as a starting point. We simply require the use of java and spring and strongly suggest using SQL and REST unless you have a good reason not to. Otherwise feel free to add or remove dependencies, restructure or even rewrite any of the provided code, change the database structure, or modify the api calls. However, please update the README with any changes to the steps required to build and run the server.

## Building and Running
Step 1. Stand up a Postgres database on localhost:32769 with username `postgres`
connecting to the database `postgres`.

Please see https://docs.docker.com/toolbox/ for an easy way to start using Docker.
Please use the `postgres` image.

Step 2.
```
# Note: requires java 8 to be installed
./gradlew clean build bootRun
```

Step 3.
Run the `example.sh` script for a quick demo of the APIs.

## Endpoints
```
GET /user
	desc: Returns info about the current user
	auth: required
	request: none
	response: {
		username: string
		private: 'false' | 'true'
	}

GET /users
	desc: Returns info about all of the users. Really just for debugging.
	auth: required
	request: none
	response: [{
		username: string
		private: 'false' | 'true'
	}]

PUT /register
	desc: Registers a new user
	auth: no
	request: {
		username: string
		password: string
		private: 'false' | 'true'
	}
	response: none

POST /user/privacyLevel
	desc: Changes the privacy level of the current user
	auth: required
	request: { private: 'true' | 'false' }
	response: none

GET /friends
	desc: Returns a list of all of the people the current user has friended
	auth: required
	request: none
	response: [ string, ... ]

PUT /friends/{username}
	desc: Adds the specified user to your friend list
	auth: required
	request: path variable
	response: none

DELETE /friends/{username}
	desc: Remove the specified user from your friend list
	auth: required
	request: path variable
	response: none

GET /feed
	desc: Gets the whole feed
	auth: required
	request: none
	response: [{
		id: integer
		author: string - a username
		content: string
		groupId: long,
		imageItems: [{image: "base64String", caption: "string"}],
		created: string - e.g, 2018-11-26T22:39:34.173+0000
	}]

GET /feed/{id}
	desc: Get a single feed item by id
	auth: required
	request: path variable
	response: {
		id: integer
		author: string - a username
		content: string
		groupId: long,
		imageItems: [{image: "base64String", caption: "string"}],
		created: string - e.g, 2018-11-26T22:39:34.173+0000
	}

PUT /feed
	desc: Add a post to the feed
	auth: required
	request: { content: string }
	response: none


GET /group/feed/{groupId}
		desc: Get a list of feed items for a particular groupId
		auth: required
		request: groupId -- see GET /groups for details
		response: [{
			id: integer
			author: string - a username
			content: string
			groupId: long,
			imageItems: [{image: "base64String", caption: "string"}],
			created: string - e.g, 2018-11-26T22:39:34.173+0000
		}]

PUT /group/feed/{groupName}
	desc: Create a group name with the user from Authentication taken as the admin
	auth: required
	request: groupName
	response: void

POST /group/feed
	desc: Create a group feed post. Must be a member or admin
	auth: required
	request: { content: string, groupId: long }

PUT /group/{groupId}/members/{username}
	desc: Add a user to a group. Authentication user must be member or admin.
	auth: required
	request: pathvariable: groupId (long), pathvariable: username (string)

GET /groups
	desc: Get a list of all groups a user belongs to
	auth: required
	request: none
```


## Tasks
Some of the tasks below are intentionally a little vague so as to give you the freedom to come up with the solution you think will work best.

1. Add an optional bio field to the users. There should be an endpoint to update it and `GET /user` should return the bio string or null.
2. Make it so that if a user's privacy level is PRIVATE, only people that the user has added to their friends list can see their posts.
3. Add support for submitting images with captions to the feed in addition to just text. That is, `GET /feed` should now return a combination of text and image/caption posts where the images are hosted by the backend.
4. Implement group/private feeds


## Bonus Tasks
These are by no means required and it will not reflect badly on you if you don't do these.

* Create an endpoint that allows a user to change their username
* Instead of auth being required for all but the register endpoint, make it optional where appropriate and change the functionality depending on whether or not they are authenticated
* Add pagination to the feed
* Create an endpoint that gets all of the users feeds as a single feed

## Submission

Please submit a link to a repository containing the code, documentation, and anything else you deem necessary
