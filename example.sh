#!/bin/bash

mnewton=bW5ld3RvbjpGaWRvMzIx
tdouglas=dGRvdWdsYXM6UGFzc3dvcmQx
harrison=aGFycmlzb246eW9kYTEyMzEyMw==

current=$(date | sed 's/ /_/g')

#Create a new user
printf "Create a user named user-$current"
curl -X PUT \
  http://localhost:8080/register \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: 77b40602-c123-7e37-49b0-c36f43eb2940' \
  -d '{
	"username": "harrison",
	"password": "yoda123123",
	"private": true,
	"bio": "I am applying for a job"
}'
printf "\n"

#Add tdouglas as a friend to the user harrison
printf "Harrison a tdouglas are now friends"
curl -X PUT \
  http://localhost:8080/friends/tdouglas \
  -H "authorization: Basic $harrison" \
  -H 'cache-control: no-cache' \
  -H 'postman-token: ca4de6f6-08c2-959d-a532-c95ea65d90b3'
printf "\n"

#for each user create a basic post
for token in $mnewton $tdouglas $harrison
do
  curl -X PUT \
  http://localhost:8080/feed \
  -H "authorization: Basic $token" \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: 11a11374-f0b5-f40d-71a9-dec9e55cc8a4' \
  -d '{
	"content": "$message"
}'
printf "\n"
done

for auth in $mnewton $tdouglas $harrison
do
  printf "Getting user informatio for $auth\n"
  curl -X GET "http://localhost:8080/user" \
  -H "authorization: Basic $auth" \
  -H 'cache-control: no-cache' \
  -H 'postman-token: c0d4b864-550a-b798-8d04-aae71e43c05d'
  printf "\n"
done

# Create a group
printf "Create a new group"
groupId=$(curl -X PUT \
  http://localhost:8080/group/feed/test \
  -H "authorization: Basic $mnewton" \
  -H 'cache-control: no-cache' \
  -H 'postman-token: c0d4b864-550a-b798-8d04-aae71e43c05d' 2> /dev/null)
printf "The new group id is $groupId\n"

# Get a group feed
printf "Getting the feed for group id $groupId\n"
curl -X GET \
  http://localhost:8080/group/feed/$groupId \
  -H "authorization: Basic $mnewton" \
  -H 'cache-control: no-cache' \
  -H 'postman-token: c0d4b864-550a-b798-8d04-aae71e43c05d'
printf "\n"

# Create a group feed post
printf "Creating a group feed post for group id $groupId\n"
curl -X POST \
  http://localhost:8080/group/feed/ \
  -H "authorization: Basic $mnewton" \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: 8bf20617-2247-681e-392e-57d9a3415e79' \
  -d '{
	"content": "Test feed post",
	"groupId": '$groupId'
}' 2> /dev/null 1> /dev/null
printf "OK\n"

# Get a group feed
printf "Getting the feed for group id $groupId\n"
curl -X GET \
  http://localhost:8080/group/feed/$groupId \
  -H "authorization: Basic $mnewton" \
  -H 'cache-control: no-cache' \
  -H 'postman-token: c0d4b864-550a-b798-8d04-aae71e43c05d'
printf "\n"

exit 0
