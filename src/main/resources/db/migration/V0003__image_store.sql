create table image_store (
  id bigserial PRIMARY KEY,
  feed_id bigserial references feed_items(id),
  caption varchar(512),
  image text,
  created TIMESTAMP NOT NULL DEFAULT now()
);

create table group_feed (
    id bigserial primary key,
    admin text,
    name text not null
);

ALTER TABLE feed_items ADD COLUMN group_id bigint references group_feed(id);

create table group_members(
    group_id bigint,
    username text references users(username),
    primary key(group_id, username)
);
