
CREATE TABLE users(
  username VARCHAR(255) PRIMARY KEY NOT NULL,
  password VARCHAR(255) NOT NULL,
  is_private boolean
);

DROP TABLE IF EXISTS feed_items;
CREATE TABLE feed_items (
  id bigserial PRIMARY KEY,
  author VARCHAR(255) NOT NULL REFERENCES users(username),
  content VARCHAR(2048) NOT NULL,
  created TIMESTAMP NOT NULL DEFAULT now()
);

DROP TABLE IF EXISTS friends;
CREATE TABLE friends(
  username VARCHAR(255) NOT NULL REFERENCES users(username),
  friend_username VARCHAR(255) NOT NULL REFERENCES users(username),
  PRIMARY KEY (username, friend_username)
);


INSERT INTO
  users(username, password, is_private)
VALUES
  ('mnewton', '$2a$10$CEvE83vib2sVuwegBLvjCO5LgahqhwLZChzjWG5XY9zZE4.kmVgY2', false), -- pass: Fido321
  ('tdouglas', '$2a$10$Y3koyjaovvr9LEdGbN10X.qVD7yTlmDIkNr/OnWwfV1icxqYgGKR2', true); -- pass: Password1
