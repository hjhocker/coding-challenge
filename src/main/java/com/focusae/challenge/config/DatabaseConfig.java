package com.focusae.challenge.config;

import com.opentable.db.postgres.embedded.EmbeddedPostgres;
import org.postgresql.ds.PGPoolingDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.flyway.FlywayDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author Michael Newton
 */
@Configuration
public class DatabaseConfig {

    @Bean
    public JdbcTemplate jdbcTemplate() {
        return new JdbcTemplate(this.dataSource());
    }

    /**
     * Expect a docker container to be running on the host that
     * maps port 32769 on the host to 5432 on the container.
     * @return
     * @throws IOException
     */
    @Bean
    public DataSource dataSource() {
        PGPoolingDataSource pgPoolingDataSource =new PGPoolingDataSource();
        pgPoolingDataSource.setDatabaseName("postgres");
        pgPoolingDataSource.setUser("postgres");
        pgPoolingDataSource.setInitialConnections(10);
        pgPoolingDataSource.setPortNumber(32769);
        pgPoolingDataSource.setServerName("localhost");
        return pgPoolingDataSource;
    }
}