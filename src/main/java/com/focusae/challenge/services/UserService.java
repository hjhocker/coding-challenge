package com.focusae.challenge.services;

import com.focusae.challenge.jpa.DBUser;
import com.focusae.challenge.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public boolean isUserPrivate(String user) {
        DBUser dbUser =userRepository.findByUsername(user);
        if (dbUser == null) {
            return false;
        }
        return dbUser.getPrivacyLevel();
    }

}
