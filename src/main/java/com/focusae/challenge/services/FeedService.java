package com.focusae.challenge.services;

import com.focusae.challenge.controllers.FeedController;
import com.focusae.challenge.jpa.DBFeedItem;
import com.focusae.challenge.jpa.DBGroupFeed;
import com.focusae.challenge.jpa.DBGroupMember;
import com.focusae.challenge.models.FeedItem;
import com.focusae.challenge.repository.FeedItemRepository;
import com.focusae.challenge.repository.GroupFeedRepository;
import com.focusae.challenge.repository.GroupMemberRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.time.Clock;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class FeedService {

    @Autowired
    private FeedItemRepository feedItemRepository;

    @Autowired
    private FriendService friendService;

    @Autowired
    private UserService userService;

    @Autowired
    private GroupFeedRepository groupFeedRepository;

    @Autowired
    private GroupMemberRepository groupMemberRepository;

    public List<FeedItem> getGroupFeed(Principal principal, Long groupId) {
        Optional<DBGroupFeed> dbGroupFeedOptional = groupFeedRepository.findById(groupId);
        if (!dbGroupFeedOptional.isPresent()) {
            return new ArrayList<>();
        }
        DBGroupFeed dbGroupFeed = dbGroupFeedOptional.get();

        checkPrincipalIsGroupMember(dbGroupFeed.getId(), principal);

        return feedItemRepository.findByGroupIdOrderByCreatedDesc(dbGroupFeed.getId()).stream()
                .map(DBFeedItem::toView)
                .collect(Collectors.toList());
    }

    public FeedItem createGroupFeedPost(Principal principal, FeedController.NewFeedItemRequest request) {
        checkPrincipalIsGroupMember(request.getGroupId(), principal);

        DBFeedItem dbFeedItem = new DBFeedItem();
        dbFeedItem.setContent(request.getContent());
        dbFeedItem.setAuthor(principal.getName());
        dbFeedItem.setGroupId(request.getGroupId());

        feedItemRepository.save(dbFeedItem);

        return dbFeedItem.toView();
    }

    public Map<Long, String> getAssignedGroups(Principal principal) {
        Map<Long, String> groups = new HashMap<>();
        String username = principal.getName();

        List<DBGroupFeed> dbGroupFeeds = groupFeedRepository.findByAdmin(username);
        for (DBGroupFeed dbGroupFeed : dbGroupFeeds) {
            groups.put(dbGroupFeed.getId(), dbGroupFeed.getName());
        }
        List<DBGroupMember> dbGroupMembers = groupMemberRepository.findByUsername(username);
        List<Long> ids = dbGroupMembers.stream().map(DBGroupMember::getGroupId).collect(Collectors.toList());
        List<DBGroupFeed> feeds = groupFeedRepository.findByIdIn(ids);
        for (DBGroupFeed dbGroupFeed : feeds) {
            groups.put(dbGroupFeed.getId(), dbGroupFeed.getName());
        }
        return groups;
    }

    public Long createGroupFeed(Principal principal, String groupName) {
        DBGroupFeed dbGroupFeed = new DBGroupFeed();
        dbGroupFeed.setAdmin(principal.getName());
        dbGroupFeed.setName(groupName);
        groupFeedRepository.save(dbGroupFeed);
        DBGroupMember member = new DBGroupMember();
        member.setGroupId(dbGroupFeed.getId());
        member.setUsername(principal.getName());
        groupMemberRepository.save(member);
        return dbGroupFeed.getId();
    }

    public String addUserToGroupFeed(Principal principal, Long groupId, String username) {
        Optional<DBGroupFeed> dbGroupFeedOptional = groupFeedRepository.findById(groupId);
        if (!dbGroupFeedOptional.isPresent()) {
            throw new RuntimeException("Group is not found");
        }

        DBGroupFeed dbGroupFeed = dbGroupFeedOptional.get();
        checkPrincipalIsGroupMember(dbGroupFeed.getId(), principal);

        DBGroupMember dbGroupMember = groupMemberRepository.findByGroupIdAndUsername(dbGroupFeed.getId(), username);
        if (dbGroupMember == null) {
            dbGroupMember = new DBGroupMember();
            dbGroupMember.setGroupId(dbGroupFeed.getId());
            dbGroupMember.setUsername(username);
            groupMemberRepository.save(dbGroupMember);
        }
        return username;
    }

    public void checkPrincipalIsGroupMember(Long groupId, Principal principal) {

        List<DBGroupMember> groupMembers = groupMemberRepository.findByGroupId(groupId);
        boolean isMember = false;

        for (DBGroupMember member : groupMembers) {
            if (member.getUsername().equals(principal.getName())) {
                isMember = true;
            }
        }

        if (!isMember) {
            throw new RuntimeException("The Principal is not a member of the group");
        }

    }

    public FeedItem createPost(FeedController.NewFeedItemRequest request, Principal principal) {
        DBFeedItem dbFeedItem = new DBFeedItem();
        dbFeedItem.setAuthor(principal.getName());
        dbFeedItem.setCreated(LocalDateTime.now(Clock.systemUTC()));
        dbFeedItem.setContent(request.getContent());
        return feedItemRepository.save(dbFeedItem).toView();
    }

    public boolean canPricipalReadUsersFeed(Principal principal, String user) {
        if (principal.getName().equals(user)) {
            return true;
        }
        if ((principal == null || StringUtils.isEmpty(user))
                ||
                (!friendService.isUserFriendsWithUser(principal.getName(), user) && userService.isUserPrivate(user))) {
            return false;
        }
        return true;
    }

    public DBFeedItem getDbFeedItem(long id, Principal principal) {
        DBFeedItem dbFeedItem =feedItemRepository.findById(id);
        if (dbFeedItem == null) {
            throw new RuntimeException("FeedItem with ID " + id + " not found");
        }
        if (canPricipalReadUsersFeed(principal, dbFeedItem.getAuthor())) {
            return dbFeedItem;
        }
        return null;
    }

    public FeedItem getFeedItem(long id, Principal principal) {
        DBFeedItem dbFeedItem =feedItemRepository.findById(id);
        if (dbFeedItem == null) {
            throw new RuntimeException("FeedItem with ID " + id + " not found");
        }
        if (canPricipalReadUsersFeed(principal, dbFeedItem.getAuthor())) {
            return dbFeedItem.toView();
        }
        return null;
    }

    public List<FeedItem> getFeedItemsForPrincipal(Principal principal) {
        return feedItemRepository.findByGroupIdIsNullOrderByCreatedDesc().stream()
                .filter(fi -> canPricipalReadUsersFeed(principal, fi.getAuthor()))
                .map(DBFeedItem::toView)
                .collect(Collectors.toList());
    }

}
