package com.focusae.challenge.services;

import com.focusae.challenge.jpa.DBFeedItem;
import com.focusae.challenge.jpa.DBFriend;
import com.focusae.challenge.repository.FriendRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class FriendService {

    @Autowired
    private FriendRepository friendRepository;

    public boolean isUserFriendsWithUser(String principal, String other) {
        List<String> friendList = friendRepository.findByUsername(principal).stream()
                .map(DBFriend::getFriendUsername)
                .collect(Collectors.toList());
        return friendList.contains(other);
    }

}
