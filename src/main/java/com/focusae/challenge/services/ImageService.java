package com.focusae.challenge.services;

import com.focusae.challenge.jpa.DBFeedItem;
import com.focusae.challenge.jpa.DBImage;
import com.focusae.challenge.models.ImageItem;
import com.focusae.challenge.repository.FeedItemRepository;
import com.focusae.challenge.repository.ImageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.security.Principal;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ImageService {

    @Autowired
    private FeedService feedService;

    @Autowired
    private FeedItemRepository feedItemRepository;

    @Autowired
    private ImageRepository imageRepository;

    public List<ImageItem> getImagesForFeedPost(Principal principal, long feedId) {
        DBFeedItem dbFeedItem = feedService.getDbFeedItem(feedId, principal);
        if (dbFeedItem == null) {
            return new ArrayList<>(1);
        }
        List<DBImage> imageSet = dbFeedItem.getImages();
        return imageSet.stream().map(DBImage::toView).collect(Collectors.toList());
    }

    public void createImage(MultipartFile multipartFile,
                            String caption,
                            Principal principal,
                            long feedId) throws IOException {

        validatePrincipalHasAccess(principal, feedId);

        String encoded = Base64.getEncoder().encodeToString(multipartFile.getBytes());

        DBFeedItem dbFeedItem = feedService.getDbFeedItem(feedId, principal);
        if (dbFeedItem == null) {
            return;
        }
        DBImage dbImage = new DBImage();
        dbImage.setImage(encoded);
        dbImage.setCaption(caption);
        imageRepository.save(dbImage);
    }

    private void validatePrincipalHasAccess(Principal principal, long feedId) {
        DBFeedItem dbFeedItem = feedItemRepository.findById(feedId);
        Objects.requireNonNull(dbFeedItem);
        if (dbFeedItem.getGroupId() == null) {
            feedService.canPricipalReadUsersFeed(principal, dbFeedItem.getAuthor());
        } else {
            feedService.checkPrincipalIsGroupMember(dbFeedItem.getGroupId(), principal);
        }
    }

}
