package com.focusae.challenge.jpa;

import com.focusae.challenge.models.ImageItem;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "image_store")
public class DBImage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "caption")
    private String caption;

    @Column(name = "image")
    private String image;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public static ImageItem toView(DBImage dbImage) {
        ImageItem imageItem = new ImageItem();
        imageItem.setImage(dbImage.getImage());
        imageItem.setCaption(dbImage.getCaption());
        return imageItem;
    }

}
