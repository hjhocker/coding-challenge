package com.focusae.challenge.jpa;

import com.focusae.challenge.controllers.UserController;
import com.focusae.challenge.models.PrivacyLevel;
import com.focusae.challenge.models.User;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "users")
public class DBUser {

    @Id
    @NotNull
    @Size(min = 3, max = 255)
    @Column(name = "username")
    private String username;

    @Size(min = 6, max = 255)
    @NotNull
    @Column(name = "password")
    private String password;

    @NotNull
    @Column(name = "is_private")
    private boolean privacyLevel;

    @Column(name = "bio")
    private String bio;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean getPrivacyLevel() {
        return privacyLevel;
    }

    public void setPrivacyLevel(boolean privacyLevel) {
        this.privacyLevel = privacyLevel;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public static DBUser fromView(UserController.RegisterRequest user, PasswordEncoder passwordEncoder) {
        DBUser dbUser = new DBUser();
        dbUser.setBio(user.getBio());
        dbUser.setUsername(user.getUsername());
        dbUser.setPassword(passwordEncoder.encode(user.getPassword()));
        dbUser.setPrivacyLevel(user.getPrivacyLevel());
        return dbUser;
    }

    public static User toView(DBUser dbUser) {
        User user = new User();
        user.setPassword(dbUser.getPassword());
        user.setPrivate(dbUser.getPrivacyLevel());
        user.setUsername(dbUser.getUsername());
        user.setBio(dbUser.getBio());
        return user;
    }

}
