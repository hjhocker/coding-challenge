package com.focusae.challenge.jpa;

import javax.persistence.*;

@Entity
@IdClass(DBGroupMemberPK.class)
@Table(name = "group_members")
public class DBGroupMember {

    @Id
    @Column(name = "group_id")
    private Long groupId;

    @Id
    @Column(name = "username")
    private String username;

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
