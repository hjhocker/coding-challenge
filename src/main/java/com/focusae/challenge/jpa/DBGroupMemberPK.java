package com.focusae.challenge.jpa;

import java.io.Serializable;

public class DBGroupMemberPK implements Serializable {

    private static final long serialVersionUID = -5373359329747307801L;

    private Long groupId;
    private String username;

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
