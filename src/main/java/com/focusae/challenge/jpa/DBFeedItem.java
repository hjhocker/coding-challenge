package com.focusae.challenge.jpa;

import com.focusae.challenge.models.FeedItem;
import com.focusae.challenge.models.ImageItem;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.Clock;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "feed_items")
public class DBFeedItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "author")
    @NotNull
    @Size(max = 255)
    private String author;

    @Column(name = "content")
    @NotNull
    @Size(max = 2048)
    private String content;

    @Column(name = "created")
    @NotNull
    private LocalDateTime created = LocalDateTime.now(Clock.systemUTC());

    @Column(name = "group_id")
    private Long groupId;

    @OneToMany
    @JoinColumns(
            @JoinColumn(name = "feed_id", referencedColumnName = "id")
    )
    private List<DBImage> images;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public List<DBImage> getImages() {
        return images;
    }

    public void setImages(List<DBImage> images) {
        this.images = images;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public FeedItem toView() {
        FeedItem feedItem = new FeedItem();
        feedItem.setAuthor(this.getAuthor());
        feedItem.setContent(this.getContent());
        feedItem.setCreated(this.getCreated());
        feedItem.setId(this.getId().intValue());
        feedItem.setGroupId(this.getGroupId());
        feedItem.setImageItems(createImageitems(images));
        return feedItem;
    }

    private List<ImageItem> createImageitems(List<DBImage> images) {

        if (images == null || images.isEmpty()) {
            return new ArrayList<>(1);
        }

        List<ImageItem> list = new ArrayList<>(images.size());

        for (DBImage dbImage : images) {
            ImageItem item = new ImageItem();
            item.setCaption(dbImage.getCaption());
            item.setImage(dbImage.getImage());
            list.add(item);
        }

        return list;

    }

}
