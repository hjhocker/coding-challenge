package com.focusae.challenge.jpa;

import java.io.Serializable;

public class DBFriendPK implements Serializable {

    private static final long serialVersionUID = -496317714207452669L;

    private String username;
    private String friendUsername;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFriendUsername() {
        return friendUsername;
    }

    public void setFriendUsername(String friendUsername) {
        this.friendUsername = friendUsername;
    }
}
