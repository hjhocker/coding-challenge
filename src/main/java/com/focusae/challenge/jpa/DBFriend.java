package com.focusae.challenge.jpa;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@IdClass(DBFriendPK.class)
@Table(name = "friends")
public class DBFriend {

    @Id
    @NotNull
    @Column(name = "username")
    private String username;

    @Id
    @NotNull
    @Column(name = "friend_username")
    private String friendUsername;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFriendUsername() {
        return friendUsername;
    }

    public void setFriendUsername(String friendUsername) {
        this.friendUsername = friendUsername;
    }

}
