package com.focusae.challenge.repository;

import com.focusae.challenge.jpa.DBGroupMember;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GroupMemberRepository extends JpaRepository<DBGroupMember, Long> {

    @Query(
            nativeQuery = true,
            value = "select * from group_members where group_id = :groupId"
    )
    List<DBGroupMember> findByGroupId(@Param("groupId") Long groupId);

    List<DBGroupMember> findByUsername(String username);

    DBGroupMember findByGroupIdAndUsername(Long groupId, String userName);

}
