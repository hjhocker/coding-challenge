package com.focusae.challenge.repository;

import com.focusae.challenge.jpa.DBImage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ImageRepository extends JpaRepository<DBImage, Long> {
}
