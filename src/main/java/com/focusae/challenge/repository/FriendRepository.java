package com.focusae.challenge.repository;

import com.focusae.challenge.jpa.DBFriend;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FriendRepository extends JpaRepository<DBFriend, String> {

    List<DBFriend> findByUsername(String username);

    DBFriend findByUsernameAndFriendUsername(String username, String friendUsername);

}
