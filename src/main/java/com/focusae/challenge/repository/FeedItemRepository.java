package com.focusae.challenge.repository;

import com.focusae.challenge.jpa.DBFeedItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FeedItemRepository extends JpaRepository<DBFeedItem, String> {

    List<DBFeedItem> findByGroupIdIsNullOrderByCreatedDesc();

    DBFeedItem findById(Long id);

    List<DBFeedItem> findByGroupIdOrderByCreatedDesc(Long groupId);

}
