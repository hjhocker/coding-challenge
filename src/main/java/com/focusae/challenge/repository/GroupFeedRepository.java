package com.focusae.challenge.repository;

import com.focusae.challenge.jpa.DBGroupFeed;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GroupFeedRepository extends JpaRepository<DBGroupFeed, Long> {

    List<DBGroupFeed> findByAdmin(String admin);

    List<DBGroupFeed> findByIdIn(List<Long> ids);

}
