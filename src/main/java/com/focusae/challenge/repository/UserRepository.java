package com.focusae.challenge.repository;

import com.focusae.challenge.jpa.DBUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<DBUser, String> {

    DBUser findByUsername(String username);

}
