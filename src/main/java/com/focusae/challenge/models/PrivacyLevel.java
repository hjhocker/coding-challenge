package com.focusae.challenge.models;

/**
 * @author Michael Newton
 */
public enum PrivacyLevel {
    PUBLIC(0),
    PRIVATE(1);

    private int dbValue;

    PrivacyLevel(int dbValue) {
        this.dbValue = dbValue;
    }

    public int getDbValue() {
        return dbValue;
    }

    public static PrivacyLevel fromDbValue(int dbValue) {
        if (dbValue == PUBLIC.dbValue) {
            return PUBLIC;
        } else {
            return PRIVATE;
        }
    }
}
