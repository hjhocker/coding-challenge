package com.focusae.challenge.controllers;

import com.focusae.challenge.jpa.DBFeedItem;
import com.focusae.challenge.models.FeedItem;
import com.focusae.challenge.repository.FeedItemRepository;
import com.focusae.challenge.services.FeedService;
import com.focusae.challenge.services.FriendService;
import com.focusae.challenge.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.security.Principal;
import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Michael Newton
 */
@RestController
public class FeedController {

    @Autowired
    private FeedService feedService;

    /**
     * Returns all items in the feed, most recent first.
     *
     * @return a list of all of the feed items
     */
    @GetMapping(path = "/feed", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<FeedItem>> getFeed(Principal principal) {
        List<FeedItem> feed = feedService.getFeedItemsForPrincipal(principal);
        return new ResponseEntity<>(feed, HttpStatus.OK);
    }

    @GetMapping(path = "/group/feed/{groupId}")
    public ResponseEntity<List<FeedItem>> getGroupFeed(Principal principal,
                                                       @PathVariable("groupId") Long groupId) {
        return new ResponseEntity<>(feedService.getGroupFeed(principal, groupId), HttpStatus.OK);
    }

    @PutMapping(path = "/group/feed/{groupName}")
    public ResponseEntity<Long> createGroupFeed(Principal principal,
                                                       @PathVariable("groupName") String groupName) {
        Long groupId = feedService.createGroupFeed(principal, groupName);
        return new ResponseEntity<Long>(groupId, HttpStatus.CREATED);
    }

    @PostMapping(path = "/group/feed")
    public ResponseEntity<FeedItem> createGroupFeedPost(Principal principal,
                                                        @RequestBody NewFeedItemRequest request) {
        return new ResponseEntity<>(feedService.createGroupFeedPost(principal, request), HttpStatus.OK);
    }

    @PutMapping(path = "/group/{groupId}/members/{username}")
    public ResponseEntity<String> addUserToGroupFeed(Principal principal,
                                                     @PathVariable("username") String username,
                                                     @PathVariable("groupId") Long groupId) {
        return new ResponseEntity<>(feedService.addUserToGroupFeed(principal, groupId, username), HttpStatus.OK);
    }

    @GetMapping(path = "/groups")
    public ResponseEntity<Map<Long, String>> getGroups(Principal principal) {
        return new ResponseEntity<>(feedService.getAssignedGroups(principal), HttpStatus.OK);
    }

    /**
     * Gets a specific feed item
     *
     * @param id - the id of the feed item
     * @return the feed item associated with the given id
     */
    @GetMapping(path = "/feed/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FeedItem> getFeedItem(@PathVariable long id, Principal principal) {
        FeedItem feedItem = feedService.getFeedItem(id, principal);
        if (feedItem == null) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        return new ResponseEntity<>(feedItem, HttpStatus.OK);
    }


    /**
     * Creates a new feed item by the authenticated user.
     *
     * @param request - request containing the content of the feed item.
     * @param principal - the principal of the authenticated user
     * @return http status 201 for created
     */
    @PutMapping(path = "/feed")
    public ResponseEntity<FeedItem> postToFeed(@RequestBody @Valid  NewFeedItemRequest request, Principal principal) {
        return new ResponseEntity<>(feedService.createPost(request, principal), HttpStatus.CREATED);
    }


    public static class NewFeedItemRequest {
        @NotNull
        private String content;

        private Long groupId;

        public Long getGroupId() {
            return groupId;
        }

        public void setGroupId(Long groupId) {
            this.groupId = groupId;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }
}
