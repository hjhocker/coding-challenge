package com.focusae.challenge.controllers;

import com.focusae.challenge.jpa.DBFriend;
import com.focusae.challenge.repository.FriendRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Michael Newton
 */
@RestController
public class FriendsController {

    private FriendRepository friendRepository;


    @Autowired
    public FriendsController(FriendRepository friendRepository) {
        this.friendRepository = friendRepository;
    }


    /**
     * Returns a list of all the people you have friended
     *
     * @param principal - the principal of the authenticated user
     * @return the list of all the people you have friended
     */
    @GetMapping(path = "/friends")
    public ResponseEntity<List<String>> getFriends(Principal principal) {
        List<DBFriend> dbFriends = friendRepository.findByUsername(principal.getName());
        return new ResponseEntity<>(dbFriends.stream().map(DBFriend::getFriendUsername).collect(Collectors.toList()), HttpStatus.OK);
    }


    /**
     * Add a user to your friends list
     *
     * @param username - username of the user being added to the friends list
     * @param principal - the principal of the authenticated user (the user adding the other user to their friends list)
     * @return http status 201 for created
     */
    @PutMapping("/friends/{username}")
    public ResponseEntity<Void> addFriend(@PathVariable String username, Principal principal) {
        DBFriend dbFriend = friendRepository.findByUsernameAndFriendUsername(principal.getName(), username);
        if (dbFriend != null) {
            return new ResponseEntity<>(HttpStatus.CREATED);
        }
        dbFriend = new DBFriend();
        dbFriend.setUsername(principal.getName());
        dbFriend.setFriendUsername(username);
        friendRepository.save(dbFriend);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }


    /**
     * Remove a user from your friends list
     * @param username - username of the user being removed from the friends list
     * @param principal - the principal of the authenticated user (the user removing the other user from their friends list)
     */
    @DeleteMapping("/friends/{username}")
    public ResponseEntity<Void> deleteFriend(@PathVariable String username, Principal principal) {
        DBFriend dbFriend = friendRepository.findByUsernameAndFriendUsername(principal.getName(), username);
        if (dbFriend == null) {
            return new ResponseEntity<>(HttpStatus.OK);
        }
        friendRepository.delete(dbFriend);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
