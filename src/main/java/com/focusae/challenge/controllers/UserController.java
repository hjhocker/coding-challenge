package com.focusae.challenge.controllers;

import com.focusae.challenge.jpa.DBUser;
import com.focusae.challenge.models.PrivacyLevel;
import com.focusae.challenge.models.User;
import com.focusae.challenge.repository.UserRepository;
import org.hibernate.exception.DataException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityExistsException;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.security.Principal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Michael Newton
 */
@RestController
public class UserController {

    private PasswordEncoder passwordEncoder;
    private UserRepository userRepository;

    @Autowired
    public UserController(PasswordEncoder passwordEncoder, UserRepository userRepository) {
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
    }

    /**
     * Returns the current users profile information
     *
     * @param principal - the principal of the current user
     * @return
     */
    @GetMapping(path = "/user", produces = MediaType.APPLICATION_JSON_VALUE)
    public User user(Principal principal) {
        DBUser dbUser = userRepository.findByUsername(principal.getName());
        return DBUser.toView(dbUser);
    }


    /**
     * Update the current users privacy level
     *
     * @param request - object containing the new privacy level
     * @param principal - the principal of the user updating their privacy level
     */
    @PostMapping("/user/privacyLevel")
    public void updatePrivacyLevel(@RequestBody UpdatePrivacyLevelRequest request, Principal principal) {
        DBUser dbUser = userRepository.findByUsername(principal.getName());
        if (dbUser == null) {
            throw new RuntimeException("The User " + principal.getName() + " does not exist");
        }
        dbUser.setPrivacyLevel(request.isPrivate());
        userRepository.save(dbUser);
    }


    /**
     * Returns all of the users in the database. For debug purposes.
     */
    @GetMapping(path = "/users", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<User> getUsers() {
        return userRepository.findAll().stream().map(DBUser::toView).collect(Collectors.toList());
    }


    /**
     * Creates a new user using the information from the request
     * @param request - the new users information, including the username, password, and privacy level
     */
    @PutMapping(path = "/register")
    public void register(@RequestBody @Valid RegisterRequest request) {
        //Note: using bcrypt for password encoding.
        DBUser dbUser = userRepository.findByUsername(request.getUsername());
        if (dbUser != null) {
            throw new EntityExistsException("The user " + request.getUsername() + "already exists");
        }
        dbUser = DBUser.fromView(request, passwordEncoder);
        userRepository.save(dbUser);
    }


    public static class RegisterRequest {

        @NotNull @Size(min = 3)
        private String username;

        @NotNull @Size(min = 6)
        private String password;

        @NotNull
        private boolean privacyLevel;

        private String bio;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public boolean getPrivacyLevel() {
            return privacyLevel;
        }

        public void setPrivacyLevel(boolean privacyLevel) {
            this.privacyLevel = privacyLevel;
        }

        public String getBio() {
            return bio;
        }

        public void setBio(String bio) {
            this.bio = bio;
        }
    }


    public static class UpdatePrivacyLevelRequest {

        @NotNull
        private boolean isPrivate;

        public boolean isPrivate() {
            return isPrivate;
        }

        public void setPrivate(boolean aPrivate) {
            isPrivate = aPrivate;
        }
    }
}
