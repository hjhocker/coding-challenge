package com.focusae.challenge.controllers;

import com.focusae.challenge.models.ImageItem;
import com.focusae.challenge.services.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.security.Principal;
import java.util.List;

@RestController
public class ImageController {

    @Autowired
    private ImageService imageService;

    @RequestMapping(value = "/images", method = RequestMethod.GET)
    public ResponseEntity<List<ImageItem>> getPostImages(Principal principal, long feedId) {
        return new ResponseEntity<>(imageService.getImagesForFeedPost(principal, feedId), HttpStatus.OK);
    }

    @RequestMapping(value = "images", method = RequestMethod.POST)
    public ResponseEntity<Void> createImagePost(@RequestParam("file") MultipartFile multipartFile,
                                                @RequestParam("caption") String caption,
                                                @RequestParam("feedId") long id,
                                                Principal principal) throws IOException {
        imageService.createImage(multipartFile, caption, principal, id);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

}
